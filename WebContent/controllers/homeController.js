Index.controller('homeController', ['$scope', '$cookieStore', 'analysisService', '$location', function( $scope, $cookieStore, analysisService, $location) {
	
	if(!$cookieStore.get('globals')){
		
		$scope.msgUserNotLogged="Unauthorized: You must <a href='index.html#/login'>Log In</a>";
		
	}else{
		
		$scope.currentUser=$cookieStore.get('globals').currentUser;
		$scope.pendingAnalysis=0;
		$scope.pendingAnalysisOld=0;
		analysisReady=0;
		$scope.analysisReady=0;
		
		
		// Actualiza la lista de análisis hasta que no quede ninguno en ejecución
		$scope.notifyEndOfAnalysis=function(){
			
			$scope.pendingAnalysisOld=$scope.pendingAnalysis;
			$scope.pendingAnalysis=0;
			
			// Obtiene la lista de análisis del usuario registrado 
			analysisService.getAnalysisList($scope.currentUser.authdata, $scope.currentUser.username)
			.success(function(data, status, headers, config) {
					    
					$scope.analysisList=data;
					
					if(!notifyRunning){	
						
						notifyRunning=true;
						
						angular.forEach ($scope.analysisList, function(value){
					    	  if(value.status== "RUNNING")
				    		  {
					    		  $scope.pendingAnalysis++;
				    		  }
					    	  
					      });
					
						if($scope.pendingAnalysisOld > $scope.pendingAnalysis)
						{
							analysisReady++;
							$scope.analysisReady=analysisReady;
							
							document.getElementById('audiotag1').play();
							
						}
						
						if($scope.pendingAnalysis > 0)
				    	{
							notifyRunning=false;
							setTimeout(function(){$scope.notifyEndOfAnalysis()},5000);
				    	  
				    	}else{
				    		notifyRunning=false;
				    		analysisReady=0;
				    	}
					}else{
						setTimeout(function(){$scope.notifyEndOfAnalysis()},5000);
					}	
				      
			}).error(function(data, status, headers, config) {
				  
					notifyRunning=false;  
					$scope.analysisListError="Error getting analysis' list.";

			});
		};
		
		// Llama a notifyEndOfAnalysis si no está ejecutándose
			
		$scope.notifyEndOfAnalysis();
		
		// Analisis Ejemplo
		$scope.analysisEx=AnalysisExample;
		
		// Ver Resultados
		$scope.viewResults= function(uuid){
			
			$location.path('/viewResults/'+uuid);
			
		};
		
		$(document).on("click", "#deleteAnalysisBtn", function () {
			
		    //get data-id attribute of the clicked element
			$scope.analysisId = $(this).data('analysis-id');
		    
		});
		
		$scope.refreshPage= function(){
			
			$('#basicModalDeleteAnalysis').modal('hide');
			
			delete $scope.analysisDeleted
			delete $scope.analysisDeletedError
			
			$scope.notifyEndOfAnalysis();
			
		};
		
		// Borra el análisis seleccionado
		$scope.deleteAnalysis= function(){
			
			analysisService.deleteAnalysis($scope.currentUser.authdata, $scope.analysisId)
			.success(function(data, status, headers, config) {
				  
				 $scope.analysisDeleted=data.value;
					
			}).error(function(data, status, headers, config) {  
				
				$scope.analysisDeletedError="Error "+data.status+" deleting analysis: "+data.value;
				 
			});
			
		};
		
		// Cancela un Análisis en ejecución
		$scope.cancelAnalysis= function(uuid){
			
			analysisService.cancelAnalysis($scope.currentUser.authdata, uuid)
			.success(function(data, status, headers, config) {
				  
				 $scope.analysisStopped=data.value;
					
			}).error(function(data, status, headers, config) {  
				
				$scope.analysisListError="Error "+data.status+" stopping analysis: "+data.value;
				 
			});
			
		};
		
	
	}
     
	
}]);