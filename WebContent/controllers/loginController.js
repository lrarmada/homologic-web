Index.controller('loginController',
	    ['authenticationService','$scope', '$cookieStore', '$location',
	     function (authenticationService,$scope, $cookieStore, $location) {
	         // Borra credenciales usuario
	         authenticationService.ClearCredentials();
	  
	         $scope.login = function () {
	            
	            authenticationService.Login($scope.username, $scope.password)       
	                 .success(function (data) {
	                	 
	                     authenticationService.SetCredentials($scope.username, data.role, $scope.password);
	                     $scope.msgLoginSuc="Login Successful";
	                     
	                     $location.path('/home');
	                 })
	                 .error(function (data, status) {
	                	 
	                	if(status == 401){
	       				  
	       				  	 $scope.msgLoginError="Unauthorized: Wrong user name/password.";
	       				  
		       			  }else{
		                     $scope.msgLoginError ="Error during Login";
		                  }
	                 });
	         };
}]);