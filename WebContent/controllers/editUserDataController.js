Index.controller('editUserDataController', ['$scope', '$cookieStore', '$routeParams', 'md5', 'userService', 'authenticationService', function( $scope, $cookieStore, $routeParams, md5, userService, authenticationService) {
	
	if(!$cookieStore.get('globals')){
		
		$scope.msgUserNotLogged="Unauthorized: You must <a href='index.html#/login'>Log In</a>";
		
	}else{
		
		$scope.currentUser=$cookieStore.get('globals').currentUser;
		
		$scope.userLogin=$routeParams.userLogin;
		$scope.visible;
		$scope.newEmail=false;
		$scope.analysisReady=analysisReady;
		
		// Obtiene información del usuario
		$scope.getUserData= function(){
			
			userService.getUserData($scope.currentUser.authdata, $scope.userLogin)
			.success(function(data, status, headers, config) {
				  
			      $scope.actualUserData=data;
			       
			}).error(function(data, status, headers, config) {
				  
				  if(status == 401){
					  $scope.MsgUDError="Not Authorized";
				  }else{
					  $scope.MsgUDError="Error getting user data.";
				  }	
			});
		};
		
		$scope.getUserData();
		
		$scope.refreshPage= function(){
			
			$('#basicModal3').modal('hide');
			
			delete $scope.visible;
			delete $scope.msgMod;
			delete $scope.msgModUserError;
			delete $scope.msgModError;
			delete $scope.MsgUDError;
			delete $scope.newEmail;
			
			delete $scope.changePass;
			delete $scope.changePassRep;
			delete $scope.changeEmail;
			
		};
		
		$scope.editEmail= function()
		{
			$scope.newEmail=!$scope.newEmail;
			delete $scope.changeEmail;
		};
		
		$scope.changeUserData = function() { 
			
			$scope.visible=true;
			
			if(($scope.changePass == undefined || $scope.changePass == "") && ($scope.changeEmail == undefined || $scope.changeEmail == "")){
				
				$scope.visible=false;
				$scope.msgModError="No changes to be saved.";
				
				
			}else
			{
				
				if($scope.changePass != $scope.changePassRep){
					
					$scope.visible=false;
					$scope.msgModError="Passwords do not match.";
					
					
				}else{
					
							// Comprobando datos modificados ...
							if($scope.changePass != undefined && $scope.changePass != "" && md5.createHash($scope.changePass) !=  $scope.actualUserData.password){
								
								 $scope.actualUserData.password=md5.createHash($scope.changePass);
								
							}
							if(($scope.changeEmail != undefined && $scope.changeEmail != "") && ($scope.changeEmail !=  $scope.actualUserData.email) ){
								
								
								 $scope.actualUserData.email=$scope.changeEmail;
					
								
							}
							
							// Modifica los datos del usuario 
							userService.modifyUserData($scope.currentUser.authdata, $scope.actualUserData.login, $scope.actualUserData.role, $scope.actualUserData.password, $scope.actualUserData.email)
							.success(function(data, status, headers, config) {
								  
								$scope.visible=false;
								
								if($scope.changePass != undefined && $scope.changePass != "" && $scope.currentUser.username== $scope.actualUserData.login){
									
									authenticationService.SetCredentials($scope.currentUser.username, $scope.currentUser.role, $scope.changePass);
								}
								
								$scope.msgMod="Changes saved successfully.";	
							
							      
							 }).error(function(data, status, headers, config) {
								  
								  $scope.visible=false;
								  
								  if(status == 401){
									  $scope.msgModUserError="Not Authorized";
								  }else{
									  $scope.msgModUserError=data.status+" Error modifying user data: "+data.value; 
								  }
								   
							});	
						
				}
			}
			
		};
		
	}
     
	
}]);
		