Index.controller('userMngmentController', ['$scope', '$cookieStore', 'userService', function( $scope, $cookieStore, userService) {
	
	if(!$cookieStore.get('globals')){
		
		$scope.msgUserNotLogged="Unauthorized: You must <a href='index.html#/login'>Log In</a>";
		
	}else{
		
		$scope.currentUser=$cookieStore.get('globals').currentUser;
		$scope.analysisReady=analysisReady;
		
		// Obtiene la lista de usuarios registrados
		$scope.getUsersList= function(){
			
			userService.getUsersList($scope.currentUser.authdata)
			.success(function(data, status, headers, config) {
				  
			      $scope.usersData=data;
			      
			 }).error(function(data, status, headers, config) {
				  
				  if(status == 401 || status == 403){
					  $scope.MsgUDError="Not Authorized";
				  }else{
					  $scope.MsgUDError="Error getting user data.";
				  }	
	
			 });
		};
		
		$scope.getUsersList();
		
		$scope.refreshPage= function(){
			
			$('#basicModal2').modal('hide');
			
			delete $scope.msgDeleteUser;
			delete $scope.msgDeleteUserSucc;
			delete $scope.invisible;
			
			$scope.getUsersList();
			
		};
		
		$(document).on("click", "#DeleteUserBtn", function () {
			
		    //get data-id attribute of the clicked element
			$scope.userLogin = $(this).data('user-login');

		    //Mostrar id en modal
		    $('.modal-body #userLogin').val($scope.userLogin);
		    
		});
		
		// Borra el usuario seleccionado
		$scope.deleteUser = function() {
			
			document.getElementById('loadingGifModal').style.display="inline";
			$scope.invisible= true;
			
			userService.deleteUser($scope.currentUser.authdata, $scope.userLogin)
			.success(function(data, status, headers, config) {
				  
				$scope.invisible= false;  
				$scope.msgDeleteUserSucc=data.value;
			      
			}).error(function(data, status, headers, config) {
				  
				  $scope.invisible= false;
				  if(status == 405)
				  {
					  $scope.msgDeleteUser="Method not allowed.";
				  }else{
					  $scope.msgDeleteUser="Error deleting user.";
				  }
			});
			
		};
	}
     
	
}]);