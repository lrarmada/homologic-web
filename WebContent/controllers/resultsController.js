Index.controller('resultsController', ['$scope', '$cookieStore', '$location', '$window', '$routeParams', 'analysisService', function( $scope, $cookieStore, $location, $window, $routeParams, analysisService) {
	
	if(!$cookieStore.get('globals')){
		
		$scope.msgUserNotLogged="Unauthorized: You must <a href='index.html#/login'>Log In</a>";
		
	}else{
		
		$scope.currentUser=$cookieStore.get('globals').currentUser;
		$scope.analysisUuid=$routeParams.analysisUuid;
		$scope.analysisReady=analysisReady;
		$scope.checkboxModel = {
		       a : '',
		       b : '',
		       c : '',
		       d : '',
		       e : ''
		};
		
		if($scope.analysisUuid != 'example'){
			// Obtiene los resultados del análisis
			analysisService.getResults($scope.currentUser.authdata, $scope.analysisUuid)
			.success(function(data, status, headers, config) {
				
			    	$scope.resultData=data;
			      
			}).error(function(data, status, headers, config) {
				  
				  $scope.resultDataError="Error getting analysis' results.";
			});
		
		}else{
			
			$scope.resultData=AnalysisExample;
		}
		
		// Filtro de parámetros de comparación: Resalta la casilla indicada por parámetros
		$scope.highlightCell = function(analysisType,result,clear) {
			
			$scope.at=analysisType;
			$scope.res=result;
			
			
			if(clear){
				// Borrar todo lo destacado
				$scope.elements=document.getElementsByName(analysisType);
				
				angular.forEach($scope.elements,function(value){
					value.style.backgroundColor="transparent";
				});
				
			}
			
			if(($scope.at && $scope.res) != undefined && document.getElementById($scope.at+","+$scope.res)!= null ){
			  
					document.getElementById($scope.at+","+$scope.res).style.backgroundColor="rgb(205, 245, 212)"
			
			  }
			 
		};
		
		// Filtro de parámetros de comparación: obtiene las casillas a resaltar (Size, IP)
		$scope.getElementToHighlight = function(analysisType,dif) {
			
			// Borrar todo lo destacado
			$scope.highlightCell(analysisType,"",true);
			
			if(dif != null){
				$scope.elements=document.getElementsByName(analysisType);
				
	
				angular.forEach($scope.elements,function(value){
					
					if(value.innerText == undefined){
						
						if(Math.abs($scope.elements[0].textContent - value.textContent) >= dif ){
							$scope.highlightCell(analysisType,value.textContent.replace(/^\s+/,'').replace(/\s+$/,''),false);
						}
						
					}else{
						
						if(Math.abs($scope.elements[0].innerText - value.innerText) >= dif ){
							$scope.highlightCell(analysisType,value.innerText,false);
						}
					}
					
				});
					
			}
			
		};
		
		// Filtro de parámetros de comparación: obtiene las casillas a resaltar (Motif)
		$scope.getMotifs = function(analysisType) {
			
			// Borrar todo lo destacado
			$scope.highlightCell(analysisType,"",true);
			
			if($scope.checkboxModel.a+$scope.checkboxModel.b+$scope.checkboxModel.c+$scope.checkboxModel.d+$scope.checkboxModel.e != ''){
				
				$scope.elements=document.getElementsByName(analysisType);
				
				angular.forEach($scope.elements,function(value){
					
					if(value.innerText == undefined){
						
						if(value.textContent.indexOf($scope.checkboxModel.a) != '-1' && value.textContent.indexOf($scope.checkboxModel.b) != '-1' && value.textContent.indexOf($scope.checkboxModel.c) != '-1' && value.textContent.indexOf($scope.checkboxModel.d) != '-1' && value.textContent.indexOf($scope.checkboxModel.e) != '-1'){
							
							$scope.highlightCell(analysisType,value.textContent.replace(/^\s+/,'').replace(/\s+$/,''),false);
						}
						
					}else{
						
						if(value.innerText.indexOf($scope.checkboxModel.a) != '-1' && value.innerText.indexOf($scope.checkboxModel.b) != '-1' && value.innerText.indexOf($scope.checkboxModel.c) != '-1' && value.innerText.indexOf($scope.checkboxModel.d) != '-1' && value.innerText.indexOf($scope.checkboxModel.e) != '-1'){
							$scope.highlightCell(analysisType,value.innerText,false);
						}
					}
					
				});
				
			}
			
		};

	}
     
	
}]);