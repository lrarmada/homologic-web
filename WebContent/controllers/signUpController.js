Index.controller('signUpController', ['$scope', '$http', 'md5', function($scope, $http, md5) {
	
	
	$scope.invisible = true;
	 
	$scope.register = function() {
		
	if($scope.passwordSU == $scope.reppasswordSU){

		$scope.invisible = false;
		
		$http({
			method:"POST",
		    url:server+"homologic/public/registration/",
		    headers:{
		    	'Accept': 'application/json,application/xml'
		    },
		    data:"{\"login\":\""+$scope.usernameSU+"\",\"password\":\""+md5.createHash($scope.passwordSU)+"\",\"email\":\""+$scope.emailSU+"\"};"
		 
		}).success(function(data, status, headers, config) {
			
			$scope.invisible = true;  
			$scope.MsgRegSucc="Registration Successful. In the next few minutes you will receive a confirmation email at <strong>"+$scope.emailSU+"</strong>";
		      
		  }).error(function(data, status, headers, config) {
			  
			  $scope.invisible = true;
			  if(data.status == 400){
				  
				  $scope.MsgReg="Error during registration: "+data.value;
				  
			  }else if(status == 400){
				  
					$scope.MsgReg="Error during registration: Invalid email address";
						
			  }else{
				  
				$scope.MsgReg="Error "+status+" during registration."+data;
					
			  }
			    
		  });
	}else{
		$scope.MsgReg="Passwords do not match.";
	}
 };

}]);