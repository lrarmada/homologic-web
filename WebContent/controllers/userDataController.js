Index.controller('userDataController', ['$scope', '$cookieStore', '$location', '$routeParams', 'genomeService', 'analysisService', 'Base64', function( $scope, $cookieStore, $location, $routeParams, genomeService, analysisService, Base64) {
	
	if(!$cookieStore.get('globals')){
		
		$scope.msgUserNotLogged="Unauthorized: You must <a href='index.html#/login'>Log In</a>";
		
	}else{
		
		$scope.currentUser=$cookieStore.get('globals').currentUser;
		$scope.userLogin=$routeParams.userLogin;
		$scope.analysisReady=analysisReady;
		
		
		$scope.refreshRefGenomes= function(){
			
			$('#basicModalDelete').modal('hide');
			$('#uploadRefGenomeModal').modal('hide');
			
			delete $scope.msgDeleteGenome;
			delete $scope.msgDeleteGenomeSucc;
			delete $scope.invisible;
			delete $scope.userDataRefGenomes;
			delete $scope.MsgRef;
			delete $scope.RefFileToUpload;
			delete $scope.MsgRefSucc;
			delete $scope.MsgUpRefError;
			delete $scope.MsgNewRefGenome;
			delete $scope.MsgRefGDError;
			
			$scope.getRefGenomes();
			
		};
		$scope.refreshTestGenomes= function(){
			
			$('#basicModal4').modal('hide');
			$('#uploadGenomeModal').modal('hide');
			
			delete $scope.msgDeleteGenome;
			delete $scope.msgDeleteGenomeSucc;
			delete $scope.invisible;
			delete $scope.userDataGenomes;
			delete $scope.MsgNewGenome;
			delete $scope.MsgUp;
			delete $scope.fileToUpload;
			delete $scope.MsgUpSucc;
			delete $scope.MsgUpError;
			delete $scope.MsgGDError;
			
			$scope.getTestGenomes();
			
		};
		
		$scope.refreshAnalysis= function(){
			
			$('#basicModalDeleteAnalysis').modal('hide');
			
			delete $scope.analysisDeleted;
			delete $scope.analysisDeletedError;
			delete $scope.invisible;
			delete $scope.analysisList;
			
			$scope.getAnalysis();
			
		};
		
		// Obtiene los Genomas de Referencia existentes
		$scope.getRefGenomes= function(){	
			genomeService.getRefGenomes($scope.currentUser.authdata)
			.success(function(data, status, headers, config) {
				
				if(data.metadata == ""){
			    	 $scope.MsgRefGDError="No Reference Genomes.";
			    }else{
			    	  $scope.userDataRefGenomes=data;
			    }
			      
			}).error(function(data, status, headers, config) {
				  
				  if(status == 401){
					  $scope.MsgNotAuth="Not Authorized";
				  }else{
					  $scope.MsgUDError="Error getting genomes data.";
				  }
			});
		};
		
		// Obtiene Genomas de Test del Usuario
		$scope.getTestGenomes= function(){
			genomeService.getUserTestGenomes($scope.currentUser.authdata, $scope.userLogin)
			.success(function(data, status, headers, config) {
				
				if(data.metadata == ""){
			    	 $scope.MsgGDError="No Test Genomes for this user yet.";
			    }else{
			    	  $scope.userDataGenomes=data;
			    }
			      
			}).error(function(data, status, headers, config) {
				  
				  if(status == 401){
					  $scope.MsgNotAuth="Not Authorized";
				  }else{
					  $scope.MsgUDError="Error getting genomes data.";
				  }
		
			});
		};
		
		// Obtiene análisis del usuario
		$scope.getAnalysis= function(){
			analysisService.getAnalysisList($scope.currentUser.authdata, $scope.userLogin)
			.success(function(data, status, headers, config) {
				  
			      $scope.analysisList=data;
			      
			      if($scope.analysisList == ''){
			    	  $scope.MsgNullComp="No analysis for this user yet.";
			      }
			        
			 }).error(function(data, status, headers, config) {
				   
				  if(status == 401){
					  $scope.MsgNotAuth="Not Authorized";
				  }else{
					  $scope.analysisListError="Error getting analysis' list.";
				  }
				  		  
			 });
		};
		
		$scope.getRefGenomes();
		$scope.getTestGenomes();
		$scope.getAnalysis();
		
		// Ver Resultados
		$scope.viewResults= function(uuid){
			
			$location.path('/viewResults/'+uuid);
			
		};
		
		$(document).on("click", "#DeleteUserBtn", function () {
			
		    //get data-id attribute of the clicked element
			$scope.analysisId = $(this).data('analysis-id');
		    
		});
		
		// Borra el análisis seleccionado
		$scope.deleteAnalysis= function(){
			
			document.getElementById('loadingGifModal').style.display="inline";
			$scope.invisible= true;
			
			analysisService.deleteAnalysis($scope.currentUser.authdata, $scope.analysisId)
			.success(function(data, status, headers, config) {
				 $scope.invisible= false; 
				 $scope.analysisDeleted=data.value;
					
			}).error(function(data, status, headers, config) {  
				
				$scope.invisible= false;
				$scope.analysisDeletedError="Error "+data.status+" deleting analysis.";
				 
			});
			
		};
		
		$(document).on("click", "#DeleteGenomeBtn", function () {
			
		    //get data-id attribute of the clicked element
			$scope.genId = $(this).data('gen-id');

		    //Mostrar id en modal
		    $('.modal-body #genId').val($scope.genId);
		    
		});
		
		// Borra un Genoma de Referencia
		$scope.deleteRefGenome= function()
		{
	
			document.getElementById('loadingGifModal').style.display="inline";
			$scope.invisible= true;
			
			genomeService.deleteRefGenome($scope.currentUser.authdata, $scope.genId)
			.success(function(data, status, headers, config) {
				  
				  $scope.invisible= false;
			      $scope.msgDeleteGenomeSucc=data.value;
			      
			}).error(function(data, status, headers, config) {
				 
				 $scope.invisible= false; 
				 $scope.msgDeleteGenome="Error deleting genome.";
			});
			
		}
		
		// Borra un Genoma de Test
		$scope.deleteTestGenome= function()
		{

			document.getElementById('loadingGifModal2').style.display="inline";
			$scope.invisible= true;
			
			genomeService.deleteTestGenome($scope.currentUser.authdata, $scope.genId)
			.success(function(data, status, headers, config) {
				  
				$scope.invisible= false;
				$scope.msgDeleteGenomeSucc=data.value;   
			      
			}).error(function(data, status, headers, config) {
				
				$scope.invisible= false;
				$scope.msgDeleteGenome="Error deleting genome.";
			});
			
		}
		
		// Asocia Nuevo Genoma Test a un usuario
		$scope.asociateFile = function(uuidFile) {
			genomeService.asociateTestGenome($scope.currentUser.authdata, $scope.genomeName, uuidFile)
			.success(function(data, status, headers, config) {
				  
				$scope.invisible = false;
				$scope.MsgNewGenome="New Test Genome created successfully.<br>Name: <strong>"+data.name+"</strong><br>Num Genes: <strong>"+data.numGenes+"</strong>";
				
			}).error(function(data, status, headers, config) {
				  
				  $scope.invisible = false;	
				  if(data.status == 500){
					  $scope.MsgUpError=data.value;
				  }else if(data.status == 400){
					  $scope.MsgUpError="Error 400 during asociation: Invalid file.";
				  }else{
					  
				 	$scope.MsgUpError="Error "+data.status+" during asociation: "+data.value;
			  	 
				  }
			});
		};
		
		// Crea un Nuevo Genoma Test
		$scope.uploadFile = function() {
			
			if($scope.fileToUpload == undefined){
				$scope.MsgUp="Select a file to upload.";
			}else{
				
				$scope.invisible = true;
				
				$scope.nameFile=$scope.fileToUpload.name;
				
				genomeService.createTestGenome($scope.currentUser.authdata, $scope.fileToUpload)
				.success(function(data, status, headers, config) {
					  
					$scope.MsgUpSucc="Successfully upload of file "+$scope.fileToUpload.name+" with name: <strong>"+$scope.genomeName+"</strong>.";
					
					$scope.asociateFile(data.value);
				      
				}).error(function(data, status, headers, config) {
					
						$scope.MsgUpError="Error "+data.status+" during upload: "+data.value;
		              
				});
			}
			
			
		
		};
		
		// Asocia Nuevo Genoma de Referencia a un usuario
		$scope.asociateReferenceFile = function(refUuidFile) {
			
			genomeService.asociateRefGenome($scope.currentUser.authdata, $scope.genomeRefName, refUuidFile)
			.success(function(data, status, headers, config) {
				  
					$scope.invisible = false;
					$scope.MsgNewRefGenome="New Reference Genome created successfully.<br>Name: <strong>"+data.name+"</strong><br>Num Genes: <strong>"+data.numGenes+"</strong>";
				
				
			 }).error(function(data, status, headers, config) {
				  	
				  $scope.invisible = false;
				  
				  if(data.status == 500){
					  
					  $scope.MsgUpRefError=data.value;
					  
				  }else if(data.status == 400){
					  
					  $scope.MsgUpRefError="Error 400 during asociation: Invalid file.";
				 
				  }else{
					  
					$scope.MsgUpRefError="Error "+data.status+" during asociation: "+data.value;
			  	  }
			  });
		};
		
		// Crea un Nuevo Genoma de Referencia
		$scope.uploadReferenceFile = function() {
			
			if($scope.RefFileToUpload == undefined){
				$scope.MsgRef="Select a file to upload.";
			}else{
				
				$scope.invisible = true;
				$scope.RefNameFile=$scope.RefFileToUpload.name;
				
				genomeService.createRefGenome($scope.currentUser.authdata, $scope.RefFileToUpload)
				.success(function(data, status, headers, config) {
					  
					$scope.MsgRefSucc="Successfully upload of reference file "+$scope.RefFileToUpload.name+" with name: <strong>"+$scope.genomeRefName+"</strong>.";
					
					$scope.asociateReferenceFile(data.value);
				      
				}).error(function(data, status, headers, config) {
					
						$scope.MsgUpRefError="Error "+data.status+" during upload: "+data.value;
		              
				});
				
			}		
		};
		
		
	}
     
	
}]);