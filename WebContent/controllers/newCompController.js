Index.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);


Index.controller('newAnalysisController', ['$scope', '$window', '$location', '$cookieStore', 'genomeService', 'analysisService' , function( $scope, $window, $location, $cookieStore, genomeService, analysisService ) {
	
	if(!$cookieStore.get('globals')){
		
		$scope.msgUserNotLogged="Unauthorized: You must <a href='index.html#/login'>Log In</a>";
		
	}else{
		
		$scope.currentUser=$cookieStore.get('globals').currentUser;
		
		$scope.invisible = true;
		$scope.analysisReady=analysisReady;
		
		// Obtiene Genomas de Referencia existentes
		genomeService.getRefGenomes($scope.currentUser.authdata)
		.success(function(data, status, headers, config) {
			  
		      $scope.userDataRefGenomes=data;
		      
		  }).error(function(data, status, headers, config) {
			  
			  $scope.MsgUDError="Error getting reference genome data.";
		  });
		
		// Obtiene Genomas de Test del usuario
		genomeService.getTestGenomes($scope.currentUser.authdata)
		.success(function(data, status, headers, config) {
			  
		      $scope.userDataGenomes=data;
		      
		  }).error(function(data, status, headers, config) {
			  
			  $scope.MsgUDError="Error getting test genome data.";
		  });
		
		$scope.refreshPage= function(){
			
			$window.location.reload();
			
		};
		
		// Asocia Nuevo Genoma de Referencia a un usuario
		$scope.asociateReferenceFile = function(refUuidFile) {
			
			genomeService.asociateRefGenome($scope.currentUser.authdata, $scope.genomeRefName, refUuidFile)
			.success(function(data, status, headers, config) {
				  
					$scope.invisible = true;
					$scope.MsgNewRefGenome="New Reference Genome created successfully.<br>Name: <strong>"+data.name+"</strong><br>Num Genes: <strong>"+data.numGenes+"</strong>";
				
				
			 }).error(function(data, status, headers, config) {
				  	
				  $scope.invisible = true;
				  
				  if(data.status == 500){
					  
					  $scope.MsgUpRefError=data.value;
					  
				  }else if(data.status == 400){
					  
					  $scope.MsgUpRefError="Error 400 during asociation: Invalid file.";
				 
				  }else{
					  
					$scope.MsgUpRefError="Error "+data.status+" during asociation: "+data.value;
			  	  }
			  });
		};
		
		// Crea un Nuevo Genoma de Referencia
		$scope.uploadReferenceFile = function() {
			
			if($scope.RefFileToUpload == undefined){
				$scope.MsgRef="Select a file to upload.";
			}else{
				
				$scope.invisible = false;
				$scope.RefNameFile=$scope.RefFileToUpload.name;
				
				genomeService.createRefGenome($scope.currentUser.authdata, $scope.RefFileToUpload)
				.success(function(data, status, headers, config) {
					  
					$scope.MsgRefSucc="Successfully upload of reference file "+$scope.RefFileToUpload.name+" with name: <strong>"+$scope.genomeRefName+"</strong>.";
					
					$scope.asociateReferenceFile(data.value);
				      
				}).error(function(data, status, headers, config) {
					
						$scope.MsgUpRefError="Error "+data.status+" during upload: "+data.value;
		              
				});
				
			}		
		};
		
		
		// Asocia Nuevo Genoma Test a un usuario
		$scope.asociateFile = function(uuidFile) {
			genomeService.asociateTestGenome($scope.currentUser.authdata, $scope.genomeName, uuidFile)
			.success(function(data, status, headers, config) {
				  
				$scope.invisible = true;
				$scope.MsgNewGenome="New Test Genome created successfully.<br>Name: <strong>"+data.name+"</strong><br>Num Genes: <strong>"+data.numGenes+"</strong>";
				
			}).error(function(data, status, headers, config) {
				  
				  $scope.invisible = true;	
				  if(data.status == 500){
					  $scope.MsgUpError=data.value;
				  }else if(data.status == 400){
					  $scope.MsgUpError="Error 400 during asociation: Invalid file.";
				  }else{
					  
				 	$scope.MsgUpError="Error "+data.status+" during asociation: "+data.value;
			  	 
				  }
			});
		};
		
		// Crea un Nuevo Genoma Test
		$scope.uploadFile = function() {
			
			if($scope.fileToUpload == undefined){
				$scope.MsgUp="Select a file to upload.";
			}else{
				
				$scope.invisible = false;
				
				$scope.nameFile=$scope.fileToUpload.name;
				
				genomeService.createTestGenome($scope.currentUser.authdata, $scope.fileToUpload)
				.success(function(data, status, headers, config) {
					  
					$scope.MsgUpSucc="Successfully upload of file "+$scope.fileToUpload.name+" with name: <strong>"+$scope.genomeName+"</strong>.";
					
					$scope.asociateFile(data.value);
				      
				}).error(function(data, status, headers, config) {
					
						$scope.MsgUpError="Error "+data.status+" during upload: "+data.value;
		              
				});
			}
			
			
		
		};
		
		// Obtiene los genes del Genoma de Test seleccionado
		$scope.getTestGenomeSelect = function() {
			
			genomeService.getGenes($scope.currentUser.authdata, $scope.TestGenSelect.id)
			.success(function(data, status, headers, config) {
				  
				  delete $scope.SelectGeneName;
			      $scope.testGenomeGenes=String(data.gene).split(",");
			        
			}).error(function(data, status, headers, config) {
				  
				  
				 $scope.MsgTGGenesError="Error getting test genome genes.";
				  		  
			});
			
		};
		
		// Obtiene la secuencia del gen seleccionado
		$scope.getGeneNameSelect = function() {
			
			genomeService.getGeneSequence($scope.currentUser.authdata, $scope.TestGenSelect.id, $scope.SelectGeneName)
			.success(function(data, status, headers, config) {
				  
			      $scope.geneSequence=data; 
			        
			}).error(function(data, status, headers, config) {
				   
				  $scope.MsgTGSeqError="Error getting gene's sequence.";
				  		  
			});
			
		};
		
		// Crea un nuevo Análisis
		$scope.newAnalysis= function(){
			analysisService.createAnalysis($scope.currentUser.authdata, $scope.nameNewComp, $scope.RefGenSelect.id, $scope.geneSequence, $scope.maxNumGenes, $scope.TestGenSelect.id, $scope.SelectGeneName)
			.success(function(data, status, headers, config) {
				  
				 $scope.analysisUuid=data.value;
				 
				 $location.path('/home');
					
			}).error(function(data, status, headers, config) {  
				
				$scope.MsgAnalysisError="Error "+data.status+" during analysis: "+data.value;
				 
			});
		};
		
		
	} 
	
	
}]);