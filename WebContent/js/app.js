'use strict';

var server = "http://localhost:8080/";
var analysisReady=0;
var notifyRunning=false;

var AnalysisExample={
						name:'Example Analysis',
						results:[{
									results:[{
										result:'980',
										analysisType:'Size'
									},
									{
										result:'0.78',
										analysisType:'Isoelectric point'
									},
									{
										result:'A,C,E',
										analysisType:'Motifs'
									}
									],
									geneName:'TestGene',
								},
								{
									results:[{
										result:'900',
										analysisType:'Size'
									},
									{
										result:'0.70',
										analysisType:'Isoelectric point'
									},
									{
										result:'A,E',
										analysisType:'Motifs'
									}
									],
									geneName:'TestGene1',
								},
								{
									results:[{
										result:'967',
										analysisType:'Size'
									},
									{
										result:'0.34',
										analysisType:'Isoelectric point'
									},
									{
										result:'A,D',
										analysisType:'Motifs'
									}
									],
									geneName:'TestGene2',
								}],
						uuid:'example',
						end:'example',
						'reference-genome':'exampleGenRef',
						'test-genome':'exampleGenTest'};

var Index = angular.module('Index',['ngRoute','angular-md5','ngCookies','ngSanitize'])

.config(function($routeProvider)
{
	$routeProvider
	.when('/login',{
		templateUrl:'views/login.html',
		controller:'loginController'
	})
	.when('/signUp',{
		templateUrl:'views/signUp.html',
		controller:'signUpController'
	})
	.when('/home',{
		templateUrl:'views/home.html',
		controller:'homeController'
	})
	.when('/userMng',{
		templateUrl:'views/userMngment.html',
		controller:'userMngmentController'
	})
	.when('/newAnalysis',{
		templateUrl:'views/newComp.html',
		controller:'newAnalysisController'
	})
	.when('/userData/:userLogin',{
		templateUrl:'views/userData.html',
		controller:'userDataController'
	})
	.when('/editUserData/:userLogin',{
		templateUrl:'views/editUserData.html',
		controller:'editUserDataController'
	})
	.when('/viewResults/:analysisUuid',{
		templateUrl:'views/viewResults.html',
		controller:'resultsController'
	})
	.otherwise({
		redirectTo:'/login'
	});
});