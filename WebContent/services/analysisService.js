Index.factory('analysisService',
    ['$http',
    function ($http) {
        var service = {};
        
        // Obtiene la lista de análisis del usuario username
        service.getAnalysisList = function (authdata, username) {
        	
        	return $http({
				method:"GET",
				headers:{
			    	'Authorization': 'Basic '+authdata
	             },
	             url:server+"homologic/api/user/"+username+"/analysis/"
			   
			});
        	
        };
        
        // Borra el análisis seleccionado
        service.deleteAnalysis= function(authdata, analysisId){
        	
			return $http({
				method:"DELETE",
				headers:{
			    	'Authorization': 'Basic '+authdata
	             },
			    url:server+"homologic/api/analysis/"+analysisId
			    
			});
        };
        
        // Cancela un Análisis en ejecución
        service.cancelAnalysis= function(authdata, uuid){
        	
        	return $http({
				method:"DELETE",
				headers:{
			    	'Authorization': 'Basic '+authdata
	             },
			    url:server+"homologic/api/analysis/task/"+uuid
			    
			});
        	
        };
       
       // Crea un nuevo Análisis
       service.createAnalysis= function(authdata, analysisName, refId, geneSequence, maxNumGenes, testId, geneName){
    	   
	    	return $http({
				method:"POST",
			    url:server+"homologic/api/analysis",
			    headers:{
			    	'Authorization': 'Basic '+authdata
	             },
			    data:"{\"name\":\""+analysisName+"\",\"reference-genome-id\":\""+refId+"\",\"test-gene-sequence\":\""+geneSequence+"\",\"max-num-genes\":\""+maxNumGenes+"\",\"test-genome-id\":\""+testId+"\",\"test-gene-name\":\""+geneName+"\"};"
			 
			});    
       
       };
       
       // Obtiene los resultados del análisis
       service.getResults= function(authdata, analysisId){
    	   return $http({
			method:"GET",
			headers:{
		    	'Authorization': 'Basic '+authdata
            },
		    url:server+"homologic/api/analysis/"+analysisId
		   
    	   });
       };
        
        return service;
 }]);