Index.factory('genomeService',
    ['$http',
    function ($http) {
        var service = {};
        
        // Obtiene Genomas de Referencia existentes
        service.getRefGenomes = function (authdata) {
        	
    		return $http({
    			method:"GET",
    			headers:{
    		    	'Authorization': 'Basic '+authdata
                 },
    		    url:server+"homologic/api/genome/reference"
    		   
    		});
        	
        };
        
        // Crea un Nuevo Genoma de Referencia
        service.createRefGenome= function (authdata, refFile) {
        	
        	var fd = new FormData();
            fd.append('genome', refFile);

            return $http.post(server + "homologic/api/genome/reference/file", fd, {
                    transformRequest: angular.identity,
                    headers : {
                            'Content-Type' : undefined,
                            'Accept' : 'application/json',
                            'Authorization' : 'Basic ' + authdata
                    }
            });
        	
        };
        
        // Asocia Nuevo Genoma de Referencia a un usuario
		service.asociateRefGenome = function(authdata, refName, refUuidFile) {
			
			return $http({
				method:"POST",
			    url:server+"homologic/api/genome/reference",
			    headers:{
			    	'Authorization': 'Basic '+authdata
	             },
			    data:"{\"name\":\""+refName+"\",\"uuid\":\""+refUuidFile+"\"};"
			 
			  });
		};
		
		// Borra un Genoma de Referencia
		service.deleteRefGenome= function(authdata, refId){
			
			return $http({
				method:"DELETE",
				headers:{
			    	'Authorization': 'Basic '+authdata
	            },
			    url:server+"homologic/api/genome/reference/"+refId
			   
			});
		};
        
        // Obtiene Genomas de Test del usuario
        service.getTestGenomes = function (authdata) {
        	return $http({
    			method:"GET",
    			headers:{
    		    	'Authorization': 'Basic '+authdata
                 },
    		    url:server+"homologic/api/genome/test"
    		   
    		});
        };
        
        // Obtiene Genomas de Test del usuario
        service.getUserTestGenomes = function (authdata, userLogin) {
        	return  $http({
    			method:"GET",
    			headers:{
    		    	'Authorization': 'Basic '+authdata
                 },
    		    url:server+"homologic/api/user/"+userLogin+"/genome/test/"
    		   
    		});
        };
        
        // Crea un Nuevo Genoma Test
        service.createTestGenome= function (authdata, testFile) {
        	
        	var fd = new FormData();
            fd.append('genome', testFile);

            return $http.post(server + "homologic/api/genome/test/file", fd, {
                    transformRequest: angular.identity,
                    headers : {
                            'Content-Type' : undefined,
                            'Accept' : 'application/json',
                            'Authorization' : 'Basic ' + authdata
                    }
            });
        };
		
        // Asocia Nuevo Genoma Test a un usuario
		service.asociateTestGenome = function(authdata, testName, testUuidFile) {
			
			return $http({
				method:"POST",
			    url:server+"homologic/api/genome/test",
			    headers:{
			    	'Authorization': 'Basic '+authdata
	             },
			    data:"{\"name\":\""+testName+"\",\"uuid\":\""+testUuidFile+"\"};"
			 
			});
		};
		
		// Borra un Genoma de Test
		service.deleteTestGenome= function(authdata, testId){
			return $http({
				method:"DELETE",
				headers:{
			    	'Authorization': 'Basic '+authdata
	            },
			    url:server+"homologic/api/genome/test/"+testId
			   
			});
		};
		
		// Obtiene los genes del Genoma de Test seleccionado
		service.getGenes = function(authdata, testId) {
			return $http({
				method:"GET",
				headers:{
			    	'Authorization': 'Basic '+authdata
	             },
			    url:server+"homologic/api/genome/test/"+testId+"/gene"
			   
			});
		};
		
		// Obtiene la secuencia del gen seleccionado
		service.getGeneSequence = function(authdata, testId, geneName) {
			return $http({
				method:"GET",
				headers:{
			    	'Authorization': 'Basic '+authdata
	             },
			    url:server+"homologic/api/genome/test/"+testId+"/gene/"+geneName
			   
			});
		};
		
        return service;
 }]);