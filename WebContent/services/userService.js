Index.factory('userService',
    ['$http',
    function ($http) {
        var service = {};
        
        // Obtiene la lista de usuarios registrados
        service.getUsersList = function (authdata) {
        	
        	return $http({
    			method:"GET",
    			headers:{
    		    	'Authorization': 'Basic '+authdata
                },
    		    url:server+"homologic/api/user/" 
    		});
        };
        
        // Obtiene información del usuario
        service.getUserData= function(authdata, userLogin){
			
        	return $http({
				method:"GET",
				headers:{
			    	'Authorization': 'Basic '+authdata
	            },
			    url:server+"homologic/api/user/"+userLogin
			   
			});
        };
        
        // Modifica los datos del usuario 
		service.modifyUserData= function(authdata, userLogin, userRole, userPass, userEmail){
	        return $http({
				method:"PUT",
				headers:{
			    	'Authorization': 'Basic '+authdata
	            },
			    url:server+"homologic/api/user/",
			    data:"{\"login\":\""+userLogin+"\",\"role\":\""+userRole+"\",\"password\":\""+userPass+"\",\"email\":\""+userEmail+"\"};"
			   
			});
		};
        
        // Borra el usuario seleccionado
		service.deleteUser = function(authdata, userLogin) {
			
			return $http({
				method:"DELETE",
				headers:{
			    	'Authorization': 'Basic '+authdata
	            },
			    url:server+"homologic/api/user/"+userLogin
			   
			});
		};
    
		
        return service;
 }]);